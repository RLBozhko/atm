package com.kursyjava.atm;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.List;
import java.util.Map;

public class ATMService {

    public static final List<Integer> nominalsList = Collections.unmodifiableList(Arrays.asList(5000, 1000, 500, 100, 50, 10, 5, 1));

    // Get cash:
    // - <currency> <amount>
    // <currency> as described above, <amount> any positive integer
    // Semantics: get the sum from the cash if possible.
    // Reply: one line per each note value, formatted as
    // <value> <number of notes>, followed with a line OK
    // ERROR if the amount is unavailable (cash remains unchanged).
    // Example:
    // - USD 2000
    // 100 20
    // OK
    public static void withdraw(PrintWriter out, String inputLine, List<CurrencyBox> currencyBoxesList) {
        String[] arguments = inputLine.split(" ");

        String currency = arguments[1];
        Integer amount = Integer.parseInt(arguments[2]);

        for (CurrencyBox currencyBox : currencyBoxesList) {
            if (currencyBox.getCurrencyCode().equals(currency)) {
                Map<Integer, Integer> currencyMap = currencyBox.getCurrencyMap();
                for (Integer nominal : nominalsList) {
                    Integer oldCount = currencyMap.getOrDefault(nominal, 0);
                    if (oldCount > 0 && amount > 0 && amount >= nominal) {
                        if (amount / nominal <= oldCount) {
                            out.println(nominal + " " + amount / nominal);
                            currencyMap.put(nominal, oldCount - amount / nominal);
                            amount = amount - (amount / nominal * nominal);
                        } else {
                            out.println(nominal + " " + oldCount);
                            currencyMap.put(nominal, 0);
                            amount = amount - (oldCount * nominal);
                        }
                    }
                    if (amount == 0) {
                        out.println("OK");
                        break;
                    }
                }
            }
        }

        if (amount != 0) {
            out.println("ERROR in isWithdraw");
        }

    }

    public static boolean isWithdraw(String inputLine, List<CurrencyBox> currencyBoxesList) {
        String[] arguments = inputLine.split(" ");
        if (arguments.length != 3) {
            return false;
        }

        // - USD 2000

        String operation = arguments[0];
        String currency = arguments[1];
        String amountText = arguments[2];

        if (!operation.equals("-")) {
            return false;
        }

        try {
            Currency.getInstance(currency);
        } catch (IllegalArgumentException e) {
            return false;
        }

        Integer amount;
        try {
            amount = Integer.parseInt(amountText);
        } catch (NumberFormatException e) {
            return false;
        }

        if (amount < 1) {
            return false;
        }

        for (CurrencyBox currencyBox : currencyBoxesList) {
            if (currencyBox.getCurrencyCode().equals(currency)) {
                Map<Integer, Integer> currencyMap = currencyBox.getCurrencyMap();
                for (Integer nominal : nominalsList) {
                    Integer oldCount = currencyMap.getOrDefault(nominal, 0);
                    if (oldCount > 0) {
                        if (amount / nominal <= oldCount) {
                            amount = amount - (amount / nominal * nominal);
                        } else {
                            amount = amount - (oldCount * nominal);
                        }
                    }
                }
            }
        }

        if (amount != 0) {
            return false;
        }

        return true;
    }

    public static boolean isAdd(String inputLine) {

        String[] arguments = inputLine.split(" ");
        if (arguments.length != 4) {
            return false;
        }

        // + RUR 1000 15

        String operation = arguments[0];
        String currency = arguments[1];
        String nominalText = arguments[2];
        String countText = arguments[3];

        if (!operation.equals("+")) {
            return false;
        }

        try {
            Currency.getInstance(currency);
        } catch (IllegalArgumentException e) {
            return false;
        }

        Integer nominal;
        try {
            nominal = Integer.parseInt(nominalText);
        } catch (NumberFormatException e) {
            return false;
        }

        if (!nominalsList.contains(nominal)) {
            return false;
        }

        Integer count;
        try {
            count = Integer.parseInt(countText);
        } catch (NumberFormatException e) {
            return false;
        }

        if (count < 1) {
            return false;
        }

        return true;

    }

    public static void add(String inputLine, List<CurrencyBox> currencyBoxesList) {
        // + <currency> <nominal> <count>
        // + RUR 1000 15
        // currencyBoxesList.

        String[] arguments = inputLine.split(" ");

        String currency = arguments[1];
        Integer nominal = Integer.parseInt(arguments[2]);
        Integer count = Integer.parseInt(arguments[3]);

        boolean isFound = false;
        for (CurrencyBox currencyBox : currencyBoxesList) {
            if (currencyBox.getCurrencyCode().equals(currency)) {
                Map<Integer, Integer> currencyMap = currencyBox.getCurrencyMap();
                Integer oldCount = currencyMap.getOrDefault(nominal, 0);
                currencyMap.put(nominal, oldCount + count);
                isFound = true;
                break;
            }
        }

        if (!isFound) {
            CurrencyBox currencyBox = new CurrencyBox(currency);
            currencyBox.getCurrencyMap().put(nominal, count);
            currencyBoxesList.add(currencyBox);
            Collections.sort(currencyBoxesList);
        }

    }

    // Print cash:
    // ?
    // Reply: one line for each currency/value pair
    // <currency> <value> <number>
    // followed by the line OK
    // Semantics: what is currently in the cash at Bankomat
    // Example: see in sample session
    public static void printCash(PrintWriter out, List<CurrencyBox> currencyBoxesList) {
        for (CurrencyBox currencyBox : currencyBoxesList) {
            for (Integer nominal : nominalsList) {
                Integer newCount = currencyBox.getCurrencyMap().getOrDefault(nominal, 0);
                if (newCount > 0) {
                    out.println(currencyBox.getCurrencyCode() + " " + nominal + " " + newCount);
                }
            }
        }
    }

}
