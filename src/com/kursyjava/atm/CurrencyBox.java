package com.kursyjava.atm;
import java.util.HashMap;
import java.util.Map;

public class CurrencyBox implements Comparable<CurrencyBox> {
    private String currencyCode;
    private Map<Integer, Integer> currencyMap;

    public CurrencyBox(String currencyCode) {
        this.currencyCode = currencyCode;
        this.currencyMap = new HashMap<Integer, Integer>();
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Map<Integer, Integer> getCurrencyMap() {
        return currencyMap;
    }

    @Override
    public int compareTo(CurrencyBox currencyBox) {
        return currencyCode.compareTo(currencyBox.currencyCode);
    }

}
