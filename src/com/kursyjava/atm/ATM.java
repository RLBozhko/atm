package com.kursyjava.atm;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ATM {

    public static void main(String[] args) throws IOException {
        List<CurrencyBox> currencyBoxesList = new ArrayList<CurrencyBox>();

        try (ServerSocket serverSocket = new ServerSocket(5000);) {
            while (true) {
                Socket clientSocket = serverSocket.accept();
                ATMRunnable atmRunnable = new ATMRunnable(clientSocket, currencyBoxesList);
                Thread thread = new Thread(atmRunnable);
                thread.start();
            }
        }
    }

}
