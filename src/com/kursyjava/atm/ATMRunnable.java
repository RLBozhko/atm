package com.kursyjava.atm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

public class ATMRunnable implements Runnable {

    private Socket clientSocket;
    private List<CurrencyBox> currencyBoxesList;

    public ATMRunnable(Socket clientSocket, List<CurrencyBox> currencyBoxesList) {
        this.clientSocket = clientSocket;
        this.currencyBoxesList = currencyBoxesList;
    }

    @Override
    public void run() {
        try (PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));) {

            String inputLine = "";
            out.println();
            while ((inputLine = in.readLine()) != null) {
                if (ATMService.isAdd(inputLine)) {
                    ATMService.add(inputLine, currencyBoxesList);
                    out.println("OK");
                } else if (ATMService.isWithdraw(inputLine, currencyBoxesList)) {
                    ATMService.withdraw(out, inputLine, currencyBoxesList);
                } else if (inputLine.equals("?")) {
                    ATMService.printCash(out, currencyBoxesList);
                    out.println("OK");
                } else if (inputLine.equals("EXIT")) {
                    out.println("BYE");
                    break;
                } else {
                    out.println("ERROR");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
